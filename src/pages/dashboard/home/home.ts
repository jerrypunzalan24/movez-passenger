import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {MenuController} from 'ionic-angular';
import {NotificationsPage} from '../notifications/notifications';
import {ProfilePage} from '../profile/profile';
import {TransactionPage} from '../transaction/transaction';
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @Component({
   selector: 'page-home',
   templateUrl: 'home.html',
 })
 export class HomePage {

   constructor(public menuCtrl : MenuController, public navCtrl: NavController, public navParams: NavParams) {
   }
 }
