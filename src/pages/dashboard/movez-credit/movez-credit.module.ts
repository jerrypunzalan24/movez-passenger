import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MovezCreditPage } from './movez-credit';

@NgModule({
  declarations: [
    MovezCreditPage,
  ],
  imports: [
    IonicPageModule.forChild(MovezCreditPage),
  ],
})
export class MovezCreditPageModule {}
