import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
/**
 * Generated class for the NewsAndPromosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector:'page-news-and-promos',
  templateUrl:'news-and-promos.html'
})
export class NewsAndPromosPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewsAndPromosPage');
  }

}
