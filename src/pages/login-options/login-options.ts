import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import {LoginPage} from '../login/login';
import $ from '../../assets/js/jquery-3.3.1.min.js';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login-options.html',
})
export class LoginOptionsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  verifyPhone(){
    this.navCtrl.push(LoginPage, {type:"phone"})
  }
  verifyEmail(){
    this.navCtrl.push(LoginPage,{type: "email"})

  }
  ngAfterViewInit(){
  }

}
