import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import {ResendEmailPage} from '../resend-email/resend-email';
import {EnterCodePage} from '../enter-code/enter-code';
/**
 * Generated class for the VerifyAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-verify-account',
  templateUrl: 'verify-account.html',
})
export class VerifyAccountPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VerifyAccountPage');
  }
  resendPage(){
    this.navCtrl.push(ResendEmailPage)
  }
  enterCodePage(){
    this.navCtrl.push(EnterCodePage)
  }

}
