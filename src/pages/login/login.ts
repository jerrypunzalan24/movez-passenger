import { Component } from '@angular/core';
import { NavController,NavParams, IonicPage } from 'ionic-angular';
import {HomePage} from '../dashboard/home/home';
declare var $: any
@Component({
  selector: 'page-home',
  templateUrl: 'login.html',
})
export class LoginPage {
  type: String
  username: String
  password: String
  constructor(public navCtrl: NavController,public navParams: NavParams) {
    this.type = navParams.get('type')
  }
  gotoDashboard(){
    this.navCtrl.setRoot(HomePage)
  }
  ngAfterViewInit(){
    $('#showpassword').click(function(){
       if($('#eyes').hasClass('ion-md-eye')){
         $('#password .text-input').attr('type','text')
         $('#eyes').removeClass('ion-md-eye').addClass('ion-md-eye-off')
       }
       else{
         $('#password .text-input').attr('type','password')
         $('#eyes').removeClass('ion-md-eye-off').addClass('ion-md-eye')
       }
    })
  }
}
