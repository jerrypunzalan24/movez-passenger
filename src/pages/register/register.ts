import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import $ from '../../assets/js/jquery-3.3.1.min.js';
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @Component({
   selector: 'page-register',
   templateUrl: 'register.html',
 })
 export class RegisterPage {

   constructor(public navCtrl: NavController, public navParams: NavParams) {
   }

   ionViewDidLoad() {
     console.log('ionViewDidLoad RegisterPage');
   }
   ngAfterViewInit(){
     $('.showpassword').click(function(){
       var target = $(this).find('ion-icon')
       if(target.hasClass('ion-md-eye')){
         target.closest('ion-item').find('.text-input').attr('type','text')
         target.removeClass('ion-md-eye').addClass('ion-md-eye-off')
       }
       else{
         target.closest('ion-item').find('.text-input').attr('type','password')
         target.removeClass('ion-md-eye-off').addClass('ion-md-eye') 
       }
     })
   }
 }
