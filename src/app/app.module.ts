import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';  
import { MyApp } from './app.component';
import {RegisterPage} from '../pages/register/register';
import {VerifyAccountPage} from'../pages/verify-account/verify-account';
import {ResendEmailPage} from '../pages/resend-email/resend-email';
import {EnterCodePage} from '../pages/enter-code/enter-code';
import {LoginPage} from '../pages/login/login';
import {LoginOptionsPage} from '../pages/login-options/login-options';
import {HomePage} from '../pages/dashboard/home/home';
import {NotificationsPage} from '../pages/dashboard/notifications/notifications';
import {ProfilePage} from '../pages/dashboard/profile/profile';
import {TransactionPage} from '../pages/dashboard/transaction/transaction';
import {NewsAndPromosPage} from '../pages/dashboard/news-and-promos/news-and-promos';
import {EmergencyContactPage} from '../pages/dashboard/emergency-contact/emergency-contact';
import {HelpAndSupportPage} from '../pages/dashboard/help-and-support/help-and-support'
import {InviteAndEarnPage} from '../pages/dashboard/invite-and-earn/invite-and-earn';
import {AboutUsPage} from '../pages/dashboard/about-us/about-us';
import {ReportIssuesPage} from '../pages/dashboard/report-issues/report-issues';
import {EditProfilePage} from '../pages/dashboard/edit-profile/edit-profile';
import {RatesPage} from '../pages/dashboard/rates/rates';
import {MovezCreditPage} from '../pages/dashboard/movez-credit/movez-credit';
@NgModule({
  declarations: [
    MyApp,
    RegisterPage,
    VerifyAccountPage,
    ResendEmailPage,
    EnterCodePage,
    LoginPage,
    LoginOptionsPage,
    HomePage,
    NotificationsPage,
    ProfilePage,
    TransactionPage,
    EmergencyContactPage,
    HelpAndSupportPage,
    NewsAndPromosPage,
    InviteAndEarnPage,
    AboutUsPage,
    EditProfilePage,
    ReportIssuesPage,
    RatesPage,
    MovezCreditPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    RegisterPage,
    VerifyAccountPage,
    ResendEmailPage,
    EnterCodePage,
    LoginPage,
    LoginOptionsPage,
    HomePage,
    NotificationsPage,
    ProfilePage,
    TransactionPage,
    EmergencyContactPage,
    HelpAndSupportPage,
    NewsAndPromosPage,
    InviteAndEarnPage,
    AboutUsPage,
    EditProfilePage,
    ReportIssuesPage,
    RatesPage,
    MovezCreditPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
