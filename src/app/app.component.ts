import { Component, ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import $ from '../assets/js/jquery-3.3.1.min.js';
import {LoginOptionsPage} from '../pages/login-options/login-options';
import {RegisterPage} from '../pages/register/register';
import {VerifyAccountPage} from'../pages/verify-account/verify-account';
import {ResendEmailPage} from '../pages/resend-email/resend-email';
import {EnterCodePage} from '../pages/enter-code/enter-code';
import {LoginPage} from '../pages/login/login';
import {HomePage} from '../pages/dashboard/home/home';
import {NotificationsPage} from '../pages/dashboard/notifications/notifications';
import {ProfilePage} from '../pages/dashboard/profile/profile';
import {TransactionPage} from '../pages/dashboard/transaction/transaction';
import {NewsAndPromosPage} from '../pages/dashboard/news-and-promos/news-and-promos';
import {InviteAndEarnPage} from '../pages/dashboard/invite-and-earn/invite-and-earn';
import {HelpAndSupportPage} from '../pages/dashboard/help-and-support/help-and-support'
import {EmergencyContactPage} from '../pages/dashboard/emergency-contact/emergency-contact';
import {MenuController} from 'ionic-angular';
import {AboutUsPage} from '../pages/dashboard/about-us/about-us';
import {ReportIssuesPage} from '../pages/dashboard/report-issues/report-issues';
import {EditProfilePage} from '../pages/dashboard/edit-profile/edit-profile';
import {NavController} from 'ionic-angular';
import {RatesPage} from '../pages/dashboard/rates/rates';
import {MovezCreditPage} from '../pages/dashboard/movez-credit/movez-credit';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('content') nav: NavController
  rootPage:any = LoginOptionsPage

  constructor(public menuCtrl : MenuController, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      splashScreen.show();
      console.log("I'm in")
    });
  }
  gotoNotif(){
    this.menuCtrl.close()
    this.rootPage = NotificationsPage
  }
  gotoTransaction(){
    this.menuCtrl.close()
    this.rootPage = TransactionPage
  }
  gotoProfile(){
    this.menuCtrl.close()
    this.rootPage = ProfilePage
  }
  gotoHelpAndSupport(){
    this.menuCtrl.close()
    this.nav.push(HelpAndSupportPage)
  }
  gotoNewsAndPromos(){
    this.menuCtrl.close()  
    this.nav.push(NewsAndPromosPage)
  }
  gotoEmergency(){
    this.menuCtrl.close()  
    this.nav.push(EmergencyContactPage)
  }
  gotoInviteAndEarn(){
    this.menuCtrl.close()
    this.nav.push(InviteAndEarnPage)
  }
  gotoAbout(){
    this.menuCtrl.close();
    this.nav.push(AboutUsPage)
  }
  gotoEditProfile(){
    this.menuCtrl.close()
    this.nav.push(EditProfilePage)
  }
  gotoReportIssues(){
    this.menuCtrl.close()
    this.nav.push(ReportIssuesPage)
  }
  gotoRates(){
    this.menuCtrl.close()
    this.nav.push(RatesPage)
  }
  gotoMovezCredit(){
    this.menuCtrl.close()
    this.nav.push(MovezCreditPage)
    
  }
}