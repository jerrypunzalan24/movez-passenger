# Installation
  - Install **Ionic and Cordova** in node by typing `npm install -g ionic cordova` 
# Instructions
1. Type `npm install` in project's directory to install missing files
2. Once it's done, type `ionic cordova build browser`
3. Then `ionic cordova run browser` to view it on browser
3. **Make sure you installed required android components in Android Studio for it to work.** 
